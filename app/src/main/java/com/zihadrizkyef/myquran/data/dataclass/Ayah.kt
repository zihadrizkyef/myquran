package com.zihadrizkyef.myquran.data.dataclass

/**
 * بِسْمِ اللهِ الرَّحْمٰنِ الرَّحِيْمِ
 * Created by zihadrizkyef on 25/03/19.
 */
data class Ayah(
    val id: Int,
    val noSurat: Int,
    val noAyat: Int,
    val teksArab: String,
    val teksIndo: String,
    val noJuz: Int
)