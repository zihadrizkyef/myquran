package com.zihadrizkyef.myquran.data

import android.app.Activity
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.support.annotation.RawRes
import android.util.Log
import com.zihadrizkyef.myquran.R
import com.zihadrizkyef.myquran.data.dataclass.Ayah
import com.zihadrizkyef.myquran.data.dataclass.Surah
import com.zihadrizkyef.myquran.util.FileUtils
import java.io.BufferedReader
import kotlin.concurrent.thread

/**
 * بِسْمِ اللهِ الرَّحْمٰنِ الرَّحِيْمِ
 * Created by zihadrizkyef on 25/03/19.
 */
class QuranDbHelper(val context: Context) : SQLiteOpenHelper(context,
    DBName, null,
    DBVersion, null) {
    companion object {
        const val TbSurat = "nama_surah"
        const val TbAyat = "teks_ayat"
        const val DBName = "Quran"
        const val DBVersion = 1
    }

    override fun onCreate(db: SQLiteDatabase?) {
        var inputStreamReader = context.resources.openRawResource(R.raw.teks_ayat).reader()
        var bufferReader = BufferedReader(inputStreamReader)
        var line = bufferReader.readLine()
        bufferReader.close()
        inputStreamReader.close()
        db?.execSQL(line)

        inputStreamReader = context.resources.openRawResource(R.raw.nama_surah).reader()
        bufferReader = BufferedReader(inputStreamReader)
        line = bufferReader.readLine()
        bufferReader.close()
        inputStreamReader.close()
        db?.execSQL(line)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.execSQL("DROP TABLE IF EXISTS $TbAyat")
        db?.execSQL("DROP TABLE IF EXISTS $TbSurat")
        onCreate(db);
    }

    fun initQuran(listener: QuranInitListener) {
        val writable = writableDatabase
        writable.execSQL("DELETE FROM $TbAyat")
        writable.execSQL("DELETE FROM $TbSurat")

        thread {
            (context as Activity).runOnUiThread { listener.onStart() }
            val raw1 = R.raw.nama_surah
            val raw2 = R.raw.teks_ayat
            val lineCount1 = FileUtils.countLineFromRaw(context, raw1)
            val lineCount2 = FileUtils.countLineFromRaw(context, raw2)
            val inputStreamReader1 = context.resources.openRawResource(raw1).reader()
            val inputStreamReader2 = context.resources.openRawResource(raw2).reader()
            val bufferReader1 = BufferedReader(inputStreamReader1)
            val bufferReader2 = BufferedReader(inputStreamReader2)

            writable.beginTransaction()
            var lineReaded = 0
            while (bufferReader1.ready()) {
                val line = bufferReader1.readLine()
                lineReaded++
                if (line.isNotBlank() && !line.startsWith("CREATE")) {
                    writable.execSQL(line)
                }
                context.runOnUiThread { listener.onProgress(lineReaded * 100 / (lineCount1+lineCount2)) }
            }
            while (bufferReader2.ready()) {
                val line = bufferReader2.readLine()
                lineReaded++
                if (line.isNotBlank() && !line.startsWith("CREATE")) {
                    writable.execSQL(line)
                }
                listener.onProgress(lineReaded * 100 / (lineCount1+lineCount2))
            }
            writable.setTransactionSuccessful()
            writable.endTransaction()

            bufferReader1.close()
            bufferReader2.close()
            inputStreamReader1.close()
            inputStreamReader2.close()
            context.runOnUiThread { listener.onFinish() }
        }
    }

    fun getSurahList(): ArrayList<Surah> {
        val surahList = arrayListOf<Surah>()
        val readable = readableDatabase
        val sql = "SELECT * FROM `$TbSurat`"
        val cursor = readable.rawQuery(sql, null)

        if (cursor.count > 0) {
            cursor.moveToFirst()
            do {
                surahList.add(Surah(
                    cursor.getInt(0),
                    cursor.getString(1),
                    cursor.getInt(2),
                    cursor.getString(3),
                    cursor.getString(4)
                ))
            } while (cursor.moveToNext())
        }

        cursor.close()
        readable.close()

        return surahList
    }

    fun getSurahName(surahIndex: Int): String {
        val cursor = readableDatabase.rawQuery("SELECT `nama_latin` FROM `$TbSurat` WHERE id=$surahIndex", null)
        if (cursor.count > 0) {
            cursor.moveToFirst()
            val surahName = cursor.getString(0)
            cursor.close()
            return surahName
        } else {
            return ""
        }
    }

    fun getAyahList(surahIndex: Int): ArrayList<Ayah> {
        val verseList = arrayListOf<Ayah>()
        val sql = "SELECT * FROM `$TbAyat` WHERE `surah` = $surahIndex"
        val readable = readableDatabase
        val cursor = readable.rawQuery(sql, null)

        if (cursor.count > 0) {
            cursor.moveToFirst()
            do {
                verseList.add(Ayah(
                    cursor.getInt(0),
                    cursor.getInt(1),
                    cursor.getInt(2),
                    cursor.getString(3),
                    cursor.getString(4),
                    cursor.getInt(5)
                ))
            } while (cursor.moveToNext())
        }

        cursor.close()
        readable.close()

        return verseList
    }
}