package com.zihadrizkyef.myquran.data

import android.content.Context
import android.content.SharedPreferences
import com.zihadrizkyef.myquran.R
import javax.crypto.KeyAgreementSpi

/**
 * بِسْمِ اللهِ الرَّحْمٰنِ الرَّحِيْمِ
 * Created by zihadrizkyef on 25/07/18.
 */

class SharedPref(private val context: Context) {
    private val PREF_NAME = context.getString(R.string.app_name) + "-shprf"
    private val KEY_INIT_QURAN_FINISHED = "init-quran-finish"
    private val KEY_SHOW_TRANSLATION = "show-translate"
    private val KEY_AYAH_SIZE = "ayah-size"
    private val KEY_TRANSLATION_SIZE = "translation-size"

    private val shrPref: SharedPreferences by lazy {context.getSharedPreferences(PREF_NAME, 0)}

    var initQuranFinished: Boolean
        get() = shrPref.getBoolean(KEY_INIT_QURAN_FINISHED, false)
        set(value) = shrPref.edit().putBoolean(KEY_INIT_QURAN_FINISHED, value).apply()

    var showTranslation: Boolean
        get() = shrPref.getBoolean(KEY_SHOW_TRANSLATION, true)
        set(value) = shrPref.edit().putBoolean(KEY_SHOW_TRANSLATION, value).apply()

    var ayahSize: Int
        get() = shrPref.getInt(KEY_AYAH_SIZE, 14)
        set(value) = shrPref.edit().putInt(KEY_AYAH_SIZE, value).apply()

    var translationSize: Int
        get() = shrPref.getInt(KEY_TRANSLATION_SIZE, 14)
        set(value) = shrPref.edit().putInt(KEY_TRANSLATION_SIZE, value).apply()
}