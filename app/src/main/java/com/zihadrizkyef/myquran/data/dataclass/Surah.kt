package com.zihadrizkyef.myquran.data.dataclass

/**
 * بِسْمِ اللهِ الرَّحْمٰنِ الرَّحِيْمِ
 * Created by zihadrizkyef on 24/04/19.
 */
data class Surah(
    val id: Int,
    val namaLatin: String,
    val jumlahAyah: Int,
    val namaArab: String,
    val terjemah: String
)