package com.zihadrizkyef.myquran.data

import com.zihadrizkyef.myquran.data.dataclass.Ayah
import com.zihadrizkyef.myquran.data.dataclass.Surah

/**
 * بِسْمِ اللهِ الرَّحْمٰنِ الرَّحِيْمِ
 * Created by zihadrizkyef on 25/03/19.
 */

class QuranRepo(private val sharedPref: SharedPref, private val db: QuranDbHelper) {
    private val context = db.context

    var quranDbIsInitialized: Boolean
        get() = sharedPref.initQuranFinished
        set(initialized) {sharedPref.initQuranFinished = initialized}

    var showTranslation: Boolean
        get() = sharedPref.showTranslation
        set(show) {sharedPref.showTranslation = show}

    var ayahSize: Int
        get() = sharedPref.ayahSize
        set(size) {sharedPref.ayahSize = size}

    var translationSize: Int
        get() = sharedPref.translationSize
        set(size) {sharedPref.translationSize = size}


    fun initQuranDb(listener: QuranInitListener) {
        db.initQuran(listener)
    }

    fun getAyahList(surahIndex: Int): ArrayList<Ayah> {
        return db.getAyahList(surahIndex)
    }

    fun getSurahName(surahIndex: Int): String {
        return db.getSurahName(surahIndex)
    }

    fun getListSurah(): ArrayList<Surah> {
        return db.getSurahList()
    }
}