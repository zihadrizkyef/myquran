package com.zihadrizkyef.myquran.data

/**
 * بِسْمِ اللهِ الرَّحْمٰنِ الرَّحِيْمِ
 * Created by zihadrizkyef on 28/03/19.
 */
abstract class QuranInitListener {
    open fun onStart() {}
    open fun onProgress(progress: Int) {}
    abstract fun onFinish()
}