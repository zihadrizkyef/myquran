package com.zihadrizkyef.myquran.util

import android.content.Context
import android.util.TypedValue



class ConvertionUtil {
    fun spToPx(sp: Float, context: Context): Int {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, sp, context.getResources().getDisplayMetrics()).toInt()
    }
}