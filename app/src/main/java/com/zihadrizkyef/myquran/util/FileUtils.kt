package com.zihadrizkyef.myquran.util

import android.content.Context
import android.support.annotation.RawRes
import java.io.BufferedReader

/**
 * بِسْمِ اللهِ الرَّحْمٰنِ الرَّحِيْمِ
 * Created by zihadrizkyef on 27/03/19.
 */
object FileUtils {
    fun countLineFromRaw(context: Context, @RawRes rawId: Int): Int {
        val inputStreamReader = context.resources.openRawResource(rawId).reader()
        val bufferReader = BufferedReader(inputStreamReader)
        var lineCount = 0
        while (bufferReader.ready()) {
            val line = bufferReader.readLine()
            if (line.isNotBlank()) {
                lineCount++
            }
        }
        return lineCount
    }
}