package com.zihadrizkyef.myquran.util

/**
 * بِسْمِ اللهِ الرَّحْمٰنِ الرَّحِيْمِ
 * Created by zihadrizkyef on 28/03/19.
 */
class MultiTaskCompleteListener() {
    var onComplete : ()->Unit = {}
    var maxTask = 0
    var taskFinished = 0

    fun taskFinishedAdd(count: Int) {
        taskFinished += count
        if (taskFinished >= maxTask) {
            onComplete()
        }
    }
}