package com.zihadrizkyef.myquran.ui.surahlistactivity

import android.util.Log
import com.zihadrizkyef.myquran.data.QuranRepo

/**
 * بِسْمِ اللهِ الرَّحْمٰنِ الرَّحِيْمِ
 * Created by zihadrizkyef on 27/03/19.
 */

class SurahListPresenter(private val repo: QuranRepo, private val view: SurahListView) {
    fun getListSurah() {
        val list = repo.getListSurah()
        view.showQuranSurahList(list)
    }
}