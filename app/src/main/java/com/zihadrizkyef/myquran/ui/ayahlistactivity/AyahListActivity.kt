package com.zihadrizkyef.myquran.ui.ayahlistactivity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.MenuItem
import com.zihadrizkyef.myquran.R
import com.zihadrizkyef.myquran.data.QuranDbHelper
import com.zihadrizkyef.myquran.data.QuranRepo
import com.zihadrizkyef.myquran.data.SharedPref
import com.zihadrizkyef.myquran.data.dataclass.Ayah
import com.zihadrizkyef.myquran.ui.AdapterAyahList
import kotlinx.android.synthetic.main.activity_ayah_list.*

class AyahListActivity : AppCompatActivity(), AyahListView {
    companion object {
        const val KeySurahIndex = "KEY_SURAH_INDEX"
    }

    private val listAyah = arrayListOf<Ayah>()
    private lateinit var adapter: AdapterAyahList

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ayah_list)

        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val sharedPref = SharedPref(this)
        val dbQuran = QuranDbHelper(this)
        val repo = QuranRepo(sharedPref, dbQuran)
        val presenter = AyahListPresenter(this, repo)

        adapter = AdapterAyahList(this, listAyah, repo)
        val linearLayoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = linearLayoutManager
        recyclerView.adapter = adapter

        val surahIndex = intent.getIntExtra(KeySurahIndex, 0)
        supportActionBar?.title = repo.getSurahName(surahIndex)
        presenter.getAyahList(surahIndex)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
            return true
        } else {
            return super.onOptionsItemSelected(item)
        }
    }

    override fun showAyah(ayahList: ArrayList<Ayah>) {
        listAyah.clear()
        listAyah.addAll(ayahList)
        adapter.notifyDataSetChanged()
    }
}
