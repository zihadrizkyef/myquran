package com.zihadrizkyef.myquran.ui.mainactivity

/**
 * بِسْمِ اللهِ الرَّحْمٰنِ الرَّحِيْمِ
 * Created by zihadrizkyef on 28/03/19.
 */
interface MainView {
    fun showProgressBar()
    fun setProgressBarProgress(progress: Int)
    fun onInitQuranFinished()
}