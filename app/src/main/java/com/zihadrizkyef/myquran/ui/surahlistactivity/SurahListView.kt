package com.zihadrizkyef.myquran.ui.surahlistactivity

import com.zihadrizkyef.myquran.data.dataclass.Surah

/**
 * بِسْمِ اللهِ الرَّحْمٰنِ الرَّحِيْمِ
 * Created by zihadrizkyef on 27/03/19.
 */
interface SurahListView {
    fun showQuranSurahList(surahList: ArrayList<Surah>)
}