package com.zihadrizkyef.myquran.ui.mainactivity

import android.util.Log
import com.zihadrizkyef.myquran.data.QuranInitListener
import com.zihadrizkyef.myquran.data.QuranRepo

/**
 * بِسْمِ اللهِ الرَّحْمٰنِ الرَّحِيْمِ
 * Created by zihadrizkyef on 28/03/19.
 */
class MainPresenter(private val view: MainView, private val repo: QuranRepo) {
    fun initQuran() {
        repo.initQuranDb(object: QuranInitListener() {
            override fun onStart() {
                Log.i("AOEU", "start")
                view.showProgressBar()
            }

            override fun onProgress(progress: Int) {
                Log.i("AOEU", "progres $progress")
                view.setProgressBarProgress(progress)
            }

            override fun onFinish() {
                Log.i("AOEU", "finish")
                view.onInitQuranFinished()
                repo.quranDbIsInitialized = true
            }
        })
    }
}