package com.zihadrizkyef.myquran.ui.settingactivity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.SeekBar
import com.zihadrizkyef.myquran.R
import com.zihadrizkyef.myquran.data.QuranDbHelper
import com.zihadrizkyef.myquran.data.QuranRepo
import com.zihadrizkyef.myquran.data.SharedPref
import kotlinx.android.synthetic.main.activity_setting.*

class SettingActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting)

        val sharedPref = SharedPref(this)
        val db = QuranDbHelper(this)

        val repo = QuranRepo(sharedPref, db)

        sw.isChecked = repo.showTranslation
        sw.setOnCheckedChangeListener { _, isChecked ->
            repo.showTranslation = isChecked
        }

        seekAyah.progress = repo.ayahSize
        seekAyah.setOnSeekBarChangeListener(object: SeekBar.OnSeekBarChangeListener {
            override fun onStartTrackingTouch(seekBar: SeekBar?) {}
            override fun onStopTrackingTouch(seekBar: SeekBar?) {}
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                repo.ayahSize = 6+progress*2
            }
        })

        seekTranslation.progress = repo.translationSize
        seekTranslation.setOnSeekBarChangeListener(object: SeekBar.OnSeekBarChangeListener {
            override fun onStartTrackingTouch(seekBar: SeekBar?) {}
            override fun onStopTrackingTouch(seekBar: SeekBar?) {}
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                repo.translationSize = 6+progress*2
            }
        })
    }
}
