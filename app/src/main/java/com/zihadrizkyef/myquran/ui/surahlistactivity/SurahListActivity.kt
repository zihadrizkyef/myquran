package com.zihadrizkyef.myquran.ui.surahlistactivity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.MenuItem
import com.zihadrizkyef.myquran.R
import com.zihadrizkyef.myquran.data.QuranDbHelper
import com.zihadrizkyef.myquran.data.QuranRepo
import com.zihadrizkyef.myquran.data.SharedPref
import com.zihadrizkyef.myquran.data.dataclass.Surah
import com.zihadrizkyef.myquran.ui.AdapterSurahList
import kotlinx.android.synthetic.main.activity_surah_list.*

class SurahListActivity : AppCompatActivity(), SurahListView {
    private lateinit var adapter: AdapterSurahList
    private val listSurah = arrayListOf<Surah>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_surah_list)

        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val sharedPref = SharedPref(this)
        val dbHelper = QuranDbHelper(this)
        val quranRepo = QuranRepo(sharedPref, dbHelper)
        val presenter = SurahListPresenter(quranRepo, this)

        val linearLayoutManager = LinearLayoutManager(this)
        adapter = AdapterSurahList(this, listSurah)
        recyclerView.layoutManager = linearLayoutManager
        recyclerView.adapter = adapter
        presenter.getListSurah()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
            return true
        } else {
            return super.onOptionsItemSelected(item)
        }
    }

    override fun showQuranSurahList(surahList: ArrayList<Surah>) {
        listSurah.clear()
        listSurah.addAll(surahList)
    }
}
