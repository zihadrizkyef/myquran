package com.zihadrizkyef.myquran.ui.ayahlistactivity

import com.zihadrizkyef.myquran.data.dataclass.Ayah

/**
 * بِسْمِ اللهِ الرَّحْمٰنِ الرَّحِيْمِ
 * Created by zihadrizkyef on 28/03/19.
 */
interface AyahListView {
    fun showAyah(ayahList: ArrayList<Ayah>)
}