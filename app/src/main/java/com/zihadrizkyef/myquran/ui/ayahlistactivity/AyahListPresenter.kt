package com.zihadrizkyef.myquran.ui.ayahlistactivity

import android.util.Log
import com.zihadrizkyef.myquran.data.QuranRepo

/**
 * بِسْمِ اللهِ الرَّحْمٰنِ الرَّحِيْمِ
 * Created by zihadrizkyef on 28/03/19.
 */
class AyahListPresenter(private val view: AyahListView, private val repo: QuranRepo) {
    fun getAyahList(surahIndex: Int) {
        val list = repo.getAyahList(surahIndex)
        view.showAyah(list)

        Log.i("AOEU", "tteohutaeushaosenuhaoeu")
    }
}