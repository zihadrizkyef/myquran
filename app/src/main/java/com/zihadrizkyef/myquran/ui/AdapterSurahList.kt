package com.zihadrizkyef.myquran.ui

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.Typeface
import android.support.v4.content.res.ResourcesCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.zihadrizkyef.myquran.R
import com.zihadrizkyef.myquran.data.dataclass.Surah
import com.zihadrizkyef.myquran.ui.ayahlistactivity.AyahListActivity

/**
 * بِسْمِ اللهِ الرَّحْمٰنِ الرَّحِيْمِ
 * Created by zihadrizkyef on 27/03/19.
 */
class AdapterSurahList(
    private val context: Context,
    private val listSurah: ArrayList<Surah>
): RecyclerView.Adapter<ViewHolderSurahList>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderSurahList {
        val view = LayoutInflater.from(context).inflate(R.layout.item_surah, parent, false)
        return ViewHolderSurahList(view) {
            val intent = Intent(context, AyahListActivity::class.java)
            intent.putExtra(AyahListActivity.KeySurahIndex, listSurah[it].id)
            context.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return listSurah.size
    }

    override fun onBindViewHolder(viewHolder: ViewHolderSurahList, position: Int) {
        viewHolder.tvNumber.text = "${listSurah[position].id}"
        viewHolder.tvName.text = listSurah[position].namaLatin
        viewHolder.tvMean.text = "(${listSurah[position].terjemah})"

        if (position % 2 == 0) {
            viewHolder.root.setBackgroundColor(Color.parseColor("#404040"))
        } else {
            viewHolder.root.setBackgroundColor(Color.parseColor("#303030"))
        }
    }
}

class ViewHolderSurahList(
        itemView: View,
        onClickListener: (position: Int)->Unit
): RecyclerView.ViewHolder(itemView) {
    val tvNumber = itemView.findViewById<TextView>(R.id.tvNumber)!!
    val tvName = itemView.findViewById<TextView>(R.id.tvName)!!
    val tvMean = itemView.findViewById<TextView>(R.id.tvMean)!!
    val root = itemView.findViewById<LinearLayout>(R.id.linearLayout)!!

    init {
        root.setOnClickListener {
            onClickListener(adapterPosition)
        }
    }
}