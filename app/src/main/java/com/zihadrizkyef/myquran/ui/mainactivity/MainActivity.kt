package com.zihadrizkyef.myquran.ui.mainactivity

import android.app.ProgressDialog
import android.content.Intent
import android.content.pm.ActivityInfo
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.zihadrizkyef.myquran.R
import com.zihadrizkyef.myquran.data.QuranDbHelper
import com.zihadrizkyef.myquran.data.QuranRepo
import com.zihadrizkyef.myquran.data.SharedPref
import com.zihadrizkyef.myquran.ui.settingactivity.SettingActivity
import com.zihadrizkyef.myquran.ui.surahlistactivity.SurahListActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), MainView {
    private lateinit var progressDialog: ProgressDialog
    private lateinit var sharedPref: SharedPref
    private lateinit var quranDbHelper: QuranDbHelper
    private lateinit var quranRepo: QuranRepo

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        sharedPref = SharedPref(this)
        quranDbHelper = QuranDbHelper(this)
        quranRepo = QuranRepo(sharedPref, quranDbHelper)
        val presenter = MainPresenter(this, quranRepo)
        if (!quranRepo.quranDbIsInitialized) {
            progressDialog = ProgressDialog(this)
            progressDialog.setTitle("Mohon tunggu sebentar")
            progressDialog.setMessage("Sedang mempersiapkan data ...")
            progressDialog.isIndeterminate = false
            progressDialog.setCancelable(false)
            progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL)

            requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
            if (requestedOrientation == ActivityInfo.SCREEN_ORIENTATION_PORTRAIT) {
                presenter.initQuran()
            }
        }

        btReadQuran.setOnClickListener {
            val intent = Intent(this, SurahListActivity::class.java)
            startActivity(intent)
        }

        btSetting.setOnClickListener {
            val intent = Intent(this, SettingActivity::class.java)
            startActivity(intent)
        }
    }

    override fun onBackPressed() {
        if (quranRepo.quranDbIsInitialized) {
            super.onBackPressed()
        }
    }

    override fun showProgressBar() {
        progressDialog.show()
    }

    override fun setProgressBarProgress(progress: Int) {
        progressDialog.progress = progress
    }

    override fun onInitQuranFinished() {
        Handler().postDelayed({progressDialog.hide()}, 1000)
    }
}
