package com.zihadrizkyef.myquran.ui

import android.content.Context
import android.graphics.Color
import android.graphics.Typeface
import android.support.v4.content.res.ResourcesCompat
import android.support.v7.widget.RecyclerView
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.text.style.TextAppearanceSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.zihadrizkyef.myquran.R
import com.zihadrizkyef.myquran.data.QuranRepo
import com.zihadrizkyef.myquran.data.dataclass.Ayah
import com.zihadrizkyef.myquran.util.CustomTypefaceSpan

/**
 * بِسْمِ اللهِ الرَّحْمٰنِ الرَّحِيْمِ
 * Created by zihadrizkyef on 27/03/19.
 */

class AdapterAyahList(
    private val context: Context,
    private val listAyah: ArrayList<Ayah>,
    private val repo: QuranRepo
) : RecyclerView.Adapter<ViewHolderAyahList>() {
    private val fontLpmq = ResourcesCompat.getFont(context, R.font.lpmq_isep_misbah)!!
    private val fontMaddina = ResourcesCompat.getFont(context, R.font.qur_std)!!

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderAyahList {
        val view = LayoutInflater.from(context).inflate(R.layout.item_ayah, parent, false)
        return ViewHolderAyahList(view, fontLpmq) {}
    }

    override fun getItemCount(): Int {
        return listAyah.size
    }

    override fun onBindViewHolder(viewHolder: ViewHolderAyahList, position: Int) {
        viewHolder.tvNumber.text = "${listAyah[position].noAyat}"
        viewHolder.tvAyah.text = createAyahSpan(listAyah[position])

        if (position % 2 == 0) {
            viewHolder.root.setBackgroundColor(Color.parseColor("#393939"))
        } else {
            viewHolder.root.setBackgroundColor(Color.parseColor("#303030"))
        }
    }

    private fun createAyahSpan(ayah: Ayah): SpannableString {
        val text = if (repo.showTranslation) {
            ayah.teksArab + " {" + ayah.noAyat + "}\n" + ayah.teksIndo
        } else {
            ayah.teksArab + "{" + ayah.noAyat + "}"
        }
        val spannable = SpannableString(text)
        val openPos = spannable.indexOfFirst { it == '{' }
        val closePos = spannable.indexOfFirst { it == '}' }
        val madina = CustomTypefaceSpan("sans-serif", fontMaddina)
        val lpqm = CustomTypefaceSpan("sans-serif", fontLpmq)
        spannable.setSpan(lpqm, 0, openPos+1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        spannable.setSpan(madina, openPos+1, closePos, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        spannable.setSpan(lpqm, closePos, closePos+1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        spannable.setSpan(TextAppearanceSpan(null, 0, repo.ayahSize, null, null), 0, closePos+1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        spannable.setSpan(TextAppearanceSpan(null, 0, repo.translationSize, null, null), closePos+1, spannable.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)

        return spannable
    }
}

class ViewHolderAyahList(itemView: View, private val arabicFont: Typeface, onClickListener: (position: Int) -> Unit) : RecyclerView.ViewHolder(itemView) {
    val root = itemView.findViewById<LinearLayout>(R.id.linearLayout)
    val tvNumber = itemView.findViewById<TextView>(R.id.tvNumber)
    val tvAyah = itemView.findViewById<TextView>(R.id.tvAyah)

    init {
        root.setOnClickListener {
            onClickListener(adapterPosition)
        }
    }
}